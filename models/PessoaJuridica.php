<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pessoa_juridica".
 *
 * @property int $id
 * @property int $cnpj
 * @property string $fantasia
 * @property string $razao_social
 * @property string $cep
 * @property string $uf
 * @property string $cidade
 * @property string $bairro
 * @property string $rua
 * @property string $numero
 * @property string $quadra
 * @property string $lote
 * @property string $complemento
 * @property int $fone
 * @property int $tipo
 * @property int $fone1
 * @property int $tipo1
 * @property string $email
 * @property string $site
 */
class PessoaJuridica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pessoa_juridica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cnpj', 'fantasia'], 'required'],
            [['cnpj', 'fone', 'tipo', 'fone1', 'tipo1'], 'default', 'value' => null],
            [['cnpj', 'fone', 'tipo', 'fone1', 'tipo1'], 'integer'],
            [['fantasia', 'razao_social', 'cep', 'uf', 'cidade', 'bairro', 'rua', 'numero', 'quadra', 'lote', 'complemento', 'email', 'site'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cnpj' => 'Cnpj',
            'fantasia' => 'Fantasia',
            'razao_social' => 'Razao Social',
            'cep' => 'Cep',
            'uf' => 'Uf',
            'cidade' => 'Cidade',
            'bairro' => 'Bairro',
            'rua' => 'Rua',
            'numero' => 'Numero',
            'quadra' => 'Quadra',
            'lote' => 'Lote',
            'complemento' => 'Complemento',
            'fone' => 'Fone',
            'tipo' => 'Tipo',
            'fone1' => 'Fone1',
            'tipo1' => 'Tipo1',
            'email' => 'Email',
            'site' => 'Site',
        ];
    }
}
