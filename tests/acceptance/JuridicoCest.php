<?php

class JuridicoCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->login('54000717081', '54000717081');
    }

    public function dadosOpcionais(AcceptanceTester $I)
    {
        $I->wantTo('Verificar se o cadastro de informações opcionais do cliente - pessoa juridica está correto');

        $I->amOnPage('/pessoa-juridica/create');
        $I->see('CLIENTES');

        $I->fillField('#pessoajuridica-cnpj', '70.146.228/0001-70');
        $I->fillField('#pessoajuridica-fantasia', 'Coca-Cola');
        $I->fillField('#pessoajuridica-razao_social', 'Coca-Cola Indústrias Ltda');

        $I->fillField('#pessoajuridica-cep','75131570');
        $I->click('//*[@id="w2"]/span/a');

        $I->fillField('#pessoajuridica-numero','850');
        $I->fillField('#pessoajuridica-complemento','Em frente ao predio');
        $I->fillField('#pessoajuridica-fone','(17) 3997-5831');
        $I->fillOutSelect2OptionField($I, '#pessoajuridica-tipo','Celular + WhatsApp');
        $I->fillField('#pessoajuridica-fone1','(17) 3997-5759');
        $I->fillOutSelect2OptionField($I, '#pessoajuridica-tipo1','WhatsApp');
        $I->fillField('#pessoajuridica-email','coca@gmail.com');
        $I->fillField('#pessoajuridica-site','https://cocacola.com');

        $I->click('//*[@id="w0"]/div[6]/button');

        $I->waitForText("Cliente (pessoa jurídica) cadastrada com sucesso!");

        $I->waitForText('CLIENTES');
        $I->seeInField('#pessoajuridica-cnpj', '70.146.228/0001-70');
        $I->seeInField('#pessoajuridica-fantasia', 'Coca-Cola');
        $I->seeInField('#pessoajuridica-razao_social', 'Coca-Cola Indústrias Ltda');
        $I->seeInField('#pessoajuridica-cep', '75131-570');
        $I->seeInField('#pessoajuridica-rua', "Rua Marreco ");
        $I->seeInField('#pessoajuridica-numero','850');
        $I->seeInField('#pessoajuridica-complemento','Em frente ao predio');
        $I->seeInField('#pessoajuridica-bairro', "Jibran El Hadj ");
        $I->seeInField('#pessoajuridica-cidade', "Anápolis");
        $I->seeInField('#pessoajuridica-uf', "GO ");
        $I->seeInField('#pessoajuridica-fone','(17) 3997-5831');
        $I->see('Celular + WhatsApp');
        $I->seeInField('#pessoajuridica-fone1','(17) 3997-5759');
        $I->see('WhatsApp');
        $I->seeInField('#pessoajuridica-email','coca@gmail.com');
        $I->seeInField('#pessoajuridica-site','https://cocacola.com');
    }
}