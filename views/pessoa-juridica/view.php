<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PessoaJuridica */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pessoa Juridicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pessoa-juridica-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cnpj',
            'fantasia:ntext',
            'razao_social:ntext',
            'cep:ntext',
            'uf:ntext',
            'cidade:ntext',
            'bairro:ntext',
            'rua:ntext',
            'numero:ntext',
            'quadra:ntext',
            'lote:ntext',
            'complemento:ntext',
            'fone',
            'tipo',
            'fone1',
            'tipo1',
            'email:ntext',
            'site:ntext',
        ],
    ]) ?>

</div>
