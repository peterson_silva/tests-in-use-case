<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PessoaJuridica */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pessoa-juridica-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cnpj')->textInput() ?>

    <?= $form->field($model, 'fantasia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'razao_social')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cep')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'uf')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cidade')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'bairro')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rua')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'numero')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'quadra')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'complemento')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fone')->textInput() ?>

    <?= $form->field($model, 'tipo')->textInput() ?>

    <?= $form->field($model, 'fone1')->textInput() ?>

    <?= $form->field($model, 'tipo1')->textInput() ?>

    <?= $form->field($model, 'email')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'site')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
