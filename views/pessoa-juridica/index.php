<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pessoa Juridicas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pessoa-juridica-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pessoa Juridica', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cnpj',
            'fantasia:ntext',
            'razao_social:ntext',
            'cep:ntext',
            //'uf:ntext',
            //'cidade:ntext',
            //'bairro:ntext',
            //'rua:ntext',
            //'numero:ntext',
            //'quadra:ntext',
            //'lote:ntext',
            //'complemento:ntext',
            //'fone',
            //'tipo',
            //'fone1',
            //'tipo1',
            //'email:ntext',
            //'site:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
